Add service provider line to config/app.php

```
'providers' => array(
    ...
    Smorken\DummyAuth\DummyAuthServiceProvider::class,
```

Set your auth driver to 'dummy' in config/auth.php
