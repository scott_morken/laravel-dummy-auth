<?php namespace Smorken\DummyAuth;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Smorken\DummyAuth\Providers\Dummy;

class DummyAuthServiceProvider extends ServiceProvider
{

    public function boot()
    {
        Auth::provider(
            'ldap',
            function ($app, array $config) {
                return new Dummy();
            }
        );
    }
}
