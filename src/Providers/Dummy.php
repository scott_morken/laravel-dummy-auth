<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/3/16
 * Time: 9:38 AM
 */

namespace Smorken\DummyAuth\Providers;

use Illuminate\Auth\GenericUser;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;

class Dummy implements UserProvider
{

    /**
     * @var GenericUser
     */
    protected $user;

    public function retrieveById($identifier)
    {
        $user = $this->baseUser();
        $user->id = $identifier;
        return $user;
    }

    public function retrieveByToken($identifier, $token)
    {
        $user = $this->baseUser();
        $user->remember_token = $token;
        return $user;
    }

    public function updateRememberToken(Authenticatable $user, $token)
    {
        //Not used
    }

    public function retrieveByCredentials(array $credentials)
    {
        $user = $this->baseUser();
        $user->username = $credentials['username'];
        return $user;
    }

    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        $this->user = $user;
        return true;
    }

    protected function baseUser()
    {
        if (!$this->user) {
            $this->user = new GenericUser(
                [
                    'id'             => 1,
                    'username'       => 'some_user',
                    'first_name'     => 'Some',
                    'last_name'      => 'User',
                    'remember_token' => null,
                    'email'          => 'some.user@example.org',
                    'password'       => md5(str_random(8)),
                ]
            );
        }
        return $this->user;
    }
}
