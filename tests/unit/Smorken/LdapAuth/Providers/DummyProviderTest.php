<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/11/14
 * Time: 1:41 PM
 */

class DummyProviderTest extends \PHPUnit\Framework\TestCase
{

    public function testRetrieveById()
    {
        $sut = new \Smorken\DummyAuth\Providers\Dummy();
        $user = $sut->retrieveById('foo');
        $this->assertEquals('foo', $user->id);
    }

    public function testRetrieveByCredentials()
    {
        $sut = new \Smorken\DummyAuth\Providers\Dummy();
        $user = $sut->retrieveByCredentials(['username' => 'foo', 'password' => 'bar']);
        $this->assertEquals('foo', $user->username);
    }

    public function testValidateIsTrue()
    {
        $sut = new \Smorken\DummyAuth\Providers\Dummy();
        $this->assertTrue($sut->validateCredentials(new \Illuminate\Auth\GenericUser([]), ['username' => 'foo', 'password' => 'bar']));
    }
}
